# Graphical Languages for Quantum Computation

Tutorial talk given the 14th of June 2024 at the Laboratoire Méthode Formelle.

Based on works done by members of the INRIA QuaCS team and their collaborators.

Compiled using xelatex
