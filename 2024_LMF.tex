% !TeX TS-program = xelatex
% !TeX spellcheck = en_US
\documentclass[aspectratio=169,table]{beamer}

\mode<presentation>
{
  %\usetheme{Warsaw}
  \usetheme{Boadilla}
  %\usetheme{Madrid}
  %\usetheme{CambridgeUS}
  %\usetheme{Rochester}
  %\usetheme{Berkeley}
  %\usetheme{PaloAlto}
  %\usetheme{default}
  %\setbeamercovered{transparent}
}

%\usepackage[frenchb]{babel}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\newcommand{\figfile}{2024_LMF}
\newtoggle{extern}
\togglefalse{extern}
%\toggletrue{extern}
\usepackage{layouts}

\input{macro.tex}



\begin{document}
\title[Quantum Graphical Languages]{Graphical Languages for Quantum Computation}

\subtitle{}

\author[de Visme]{ \centering Marc de Visme\\\textit{QuaCS}}

\institute[]
{}

\date{14th of June 2024}
\setbeamertemplate{navigation symbols}{} % Pour effacer la ``barre de navigation'' en bas des slides
%\scriptsize \insertframenumber~/~\inserttotalframenumber} 
\setbeamertemplate{blocks} [rounded] [shadow=false]
\newcommand{\backupbegin}{
	\newcounter{finalframe}
	\setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
	\setcounter{framenumber}{\value{finalframe}}
}

%\AtBeginSection[]
%{
%  \begin{frame}<beamer>
%    \frametitle{Presentation Plan}
%    \tableofcontents[hideothersubsections]
%  \end{frame}
%}

\begin{frame}
  \titlepage
\end{frame}



\section{Quantum Programming}
\begin{frame}
	\frametitle{Quantum Programming}
	\twocl{
		\begin{exampleblock}{Overview of a quantum bit}	\centering	
			\includegraphics[width=6cm]{\figfile/newmeas.pdf}
		\end{exampleblock}
		\begin{itemize}
			%\item \only<3->{Multiple quantum bits}\only<1-2>{Quantum bit}: $\qbit\only<3->{^{\tensor n}}$
			\item {New}: $\bit \to \qbit$
			\item {Measure}: $\qbit \to \bit$ (with probabilities)
			\item Unitary: $\qbit{^{\otimes n}} \to \qbit{^{\otimes n}}$
			%\item<3-> Superposition and entanglement
		\end{itemize}
	}{
		\begin{block}<3->{A quantum bit $\qbit = \CC^2$}
			$\alpha \ket{0} + \beta \ket{1} = \begin{pmatrix}
			\alpha \\ \beta 
			\end{pmatrix} \in \CC^2  \text{ with } |\alpha|^2 + |\beta|^2 = 1$
		\end{block}
		
		\begin{block}<4->{Multiple quantum bits $\qbit^{\otimes n} = \CC^{2^n}$}
			$\gamma_0 \ket{000} + \gamma_1 \ket{001} + \dots = \begin{pmatrix}
			\gamma_0 \\ \gamma_1 \\ \dots 
			\end{pmatrix} \in \CC^8$ with $|\gamma_0|^2 + |\gamma_1|^2 + \dots = 1$
		\end{block}
	}
	\begin{alertblock}<2->{No Cloning Theorem}
		We cannot create a independent copy of a \qbit
	\end{alertblock}
\end{frame}

\begin{frame}
	\frametitle{Quantum vs Probabilistic}
	\begin{center}
		\begin{tabular}{ccc}
			\uncover<2->{\includegraphics[scale=2,align=c]{\figfile/Quantum_p.pdf}}  & \uncover<2->{$\xmapsto{\text{Measure}} \qquad\qquad$ }& 
			\includegraphics[scale=2,align=c]{\figfile/Proba_p.pdf} \\
			\uncover<2->{$\sqrt{p} \cdot e^{\bfup{i}\alpha} \ket{0} + \sqrt{1-p} \cdot e^{\bfup{i}(\alpha+\theta)} \ket{1}$} &&\\
			\uncover<4->{$\downarrow$} & & \uncover<3->{$\downarrow$} \\
			\uncover<4->{\includegraphics[scale=2,align=c]{\figfile/Quantum_f_p.pdf}}  &  & 
			\uncover<3->{\includegraphics[scale=2,align=c]{\figfile/Proba_f_p.pdf}} \\
		\end{tabular}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Why not Both? Impure Quantum Computation}
	\twocl[-1cm]{
	\begin{block}{Pure Computation}
		\begin{description}
			\item[Data] Vectors of $\mathbb{C}^n$
			\item[Programs] Matrices over $\mathbb{C}$\\(usually unitaries)
		\end{description}
\end{block}
\begin{itemize}
	\item No measurement.
	\item No way to discard unwanted data.
	\item No probabilities.
\end{itemize}	
}{
\begin{block}<2->{Impure Computation}
	\begin{description}
		\item[Data] "Density matrices"\\
		$\approx$ Probabilistic sums of vectors of $\mathbb{C}^n$
		\item[Programs] "Completely positive maps"
	\end{description}
\end{block}
\begin{itemize}
\item<2-> Has measurement.
\item<2-> Can discard unwanted data.
\item<2-> Has probabilities.
\end{itemize}	
}
\begin{alertblock}<3->{Stinespring's dilatation theorem}
\centering Impure = Pure + Discard (in an almost unique way)
\end{alertblock}
\end{frame}

\begin{frame}
	\frametitle{What is a Graphical Language?}
	\twocl{
		\begin{block}{Logical Circuit}\centering
		\includegraphics[scale=.18]{\figfile/LogicCircuit.png}
	\end{block}
		}{
		\begin{block}<2->{Quantum Circuit}\centering
		\scalebox{2}{\tikzfig{circuit-example}}\\[0.5cm]
		$ \bb{\text{Circuit}} = \begin{blockarray}{ccccc}
		& \gris{00} & \gris{01} & \gris{10} & \gris{11}\\
		\begin{block}{c(cccc)}
		\gris{00} & \frac{1}{\sqrt{2}} & 0 &0& \frac{1}{\sqrt{2}} \\
		\gris{01} & 0 & \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}  & 0\\
		\gris{10} & \frac{1}{\sqrt{2}} & 0& 0& \frac{-1}{\sqrt{2}} \\
		\gris{11}  & 0  & \frac{1}{\sqrt{2}}  & \frac{-1}{\sqrt{2}}  & 0\\
		\end{block}
		\end{blockarray}$			
		\end{block}}
	
\end{frame}

\begin{frame}
	\frametitle{What is the Point of Graphical Languages?}
	\twocl{\begin{alertblock}{Equational Theory}\centering
			\tikzfig{eq_exchange}\\
			\tikzfig{eq_hadamard_involutif}	\\
			$\dots$\\
			$\implies$ compilation, optimisation, etc	
		\end{alertblock}		
	}{\begin{itemize}
		\item<2-> Graphical properties (graph theory, causality of computation, etc)
		\item<2-> Proofs using category theory (monoidal categories, PROP, etc)
		\item<2-> Sometimes easier to understand and teach
\end{itemize}}
\end{frame}

\begin{frame}
	\frametitle{(Unitary) Quantum Circuits in Details}	
	\begin{ctabular}{cccc}
		\textbf{Name} & \textbf{Diagram} & \textbf{Type} & \textbf{Semantics} \\\hline
		 Hadamard & \tikzfig{gate-H}& $\qbit \to \qbit$ & $\begin{pmatrix}
		 \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}} & \frac{-1}{\sqrt{2}} \\
		 \end{pmatrix}$ \\
		 Phase $\phi$ &\tikzfig{gate-Phase}& $\qbit \to \qbit$ & $\begin{pmatrix}
		 1 & 0 \\ 0 & \textbf{e}^{\textbf{i}\phi} \\
		 \end{pmatrix}$ \\
		 Controlled Not &\tikzfig{gate-CNOT}& $\qbit^{\otimes 2} \to \qbit^{\otimes 2}$ & $\begin{pmatrix} 1&0&0&0\\0&1&0&0\\0&0&0&1\\0&0&1&0\end{pmatrix}$\\
		 \textcolor{gray}{Global Phase $\phi$} &\tikzfig{gate-Global}& \textcolor{gray}{$\qbit^{\otimes 0} \to \qbit^{\otimes 0}$} & \textcolor{gray}{$(\textbf{e}^{\textbf{i}\phi})$}
	\end{ctabular} 
	\begin{block}<2->{Universality}
		Every unitary transformation $\qbit^{\otimes n} \to \qbit^{\otimes n}$ can be obtained from those generators.
	\end{block}
\end{frame}


\begin{frame}
	\frametitle{Decomposing the Definition of a Graphical Language}
	\twocl[-0.5cm]{
	\begin{block}{Wires}
		List of wires in parallel\only<3->{ \alert<3->{= Objects}}.
		\begin{itemize}
			\item Each wire is the same (e.g. a qubit).
			\item Or each wire has its own "color"\\(e.g. the Hilbert space $\mathbb{C}^n$).
		\end{itemize}
	\end{block}	
	\begin{block}{Diagrams}
		\begin{itemize}
			\item Form a list of wires to another.\\ \uncover<3->{\alert{Morphisms from an object to another.}}
			\item Parallel/sequential composition of elementary diagrams.
		\end{itemize}
	\end{block}
	
}{

\begin{exampleblock}{}\centering
	\scalebox{2}{\tikzfig{circuit-example}}
\end{exampleblock}
	
	\begin{alertblock}<3->{Category}
		Diagrams can be composed sequentially${}^*$.
	\end{alertblock}
\begin{alertblock}<4->{Symmetric Monoidal Category}And diagrams can also be composed in parallel${}^*$.
\end{alertblock}

\begin{alertblock}<4->{Colored PROP}
	And the objects are actually just lists of colors${}^*$.
\end{alertblock}
}
\begin{center}\uncover<3->{\textit{${}^*$ Additional conditions may apply. All categorical definitions are subjects to coherence laws.}}\end{center}
\end{frame}


\begin{frame}
	\frametitle{Complete Equational Theory fo Quantum Circuits [Clément et al. 2023]}
	\centering
	\includegraphics[height=6cm,trim={4cm 20.4cm 3.5cm 3.7cm},clip]{\figfile/CircuitsQuantiquesP6.pdf}
	\begin{center}
	\textit{The $R_X(\varphi)$ box and the multicontrolled operations are macros.}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Main Properties we Expect from a Graphical Language}
	\twocl{
	\begin{block}{Compositionality (of a Semantics)}
		A diagram's semantics is obtained inductively:
		\[\bb{\tikzfig{f-seq-g}} = \bb{g} \circ \bb{f} \]
		\[\bb{\tikzfig{f-tensor-g}} = \bb{f} \otimes \bb{g} \]
	\end{block}	
		
	\begin{block}<2->{Universality (of a Semantics)}
		Every element of the codomain of the semantics can be obtained:
		\[ \forall s, \exists f, \bb{f} = s \]
	\end{block}
	}{
	\begin{block}<3->{Soundness (of an Equational Theory)}
		Local rewriting preserve the semantics:
		\[ \text{Whenever } \text{Eq} \vdash f = g \text{ we have } \bb{f} = \bb{g} \]
\end{block}
\begin{alertblock}<4->{Completeness (of an Equational Theory)}
	Every sound equation can be derived:
	\[ \text{Whenever } \bb{f} = \bb{g} \text{ we have }  \text{Eq} \vdash f = g\]
\end{alertblock}
\begin{exampleblock}<5->{Other Properties}
	\begin{itemize}
		\item Minimality
		\item Normal form
		\item Reduction system / Optimisation
	\end{itemize}
\end{exampleblock}}

\end{frame}

\begin{frame}
	\frametitle{The ZX-Calculus}
	\twocl[-0.5cm]{
		\begin{exampleblock}{The Z-spider}
		\[
		\tikzfig{green-node}\]
	\[ \begin{matrix} \ket{0} & \mapsto & \ket{0} \otimes \dots \otimes \ket{0} \\ \ket{1} & \mapsto & \textbf{e}^{\textbf{i}\varphi}\ket{1} \otimes \dots \otimes \ket{1} \end{matrix} \]
	\only<-3>{Only $\ket{0}$ and $\ket{1}$ are copied, superposition and entanglement are \textbf{not} copied.}
\end{exampleblock}	
	}{
	\begin{alertblock}<2->{The X-spider}
		\[
		\tikzfig{red-node}\]
		\small
		\[ \begin{matrix} \left(\frac{\ket{0}}{\sqrt{2}} + \frac{\ket{1}}{\sqrt{2}}\right) & \mapsto &  \left(\frac{\ket{0}}{\sqrt{2}} + \frac{\ket{1}}{\sqrt{2}}\right) \otimes \dots \otimes  \left(\frac{\ket{0}}{\sqrt{2}} + \frac{\ket{1}}{\sqrt{2}}\right)\\  \left(\frac{\ket{0}}{\sqrt{2}} - \frac{\ket{1}}{\sqrt{2}}\right) & \mapsto &\textbf{e}^{\textbf{i}\varphi}\left(\frac{\ket{0}}{\sqrt{2}} - \frac{\ket{1}}{\sqrt{2}}\right) \otimes \dots \otimes \left(\frac{\ket{0}}{\sqrt{2}} - \frac{\ket{1}}{\sqrt{2}}\right) \end{matrix} \]
		\normalsize
		\only<-3>{Classical $\ket{0}$ and $\ket{1}$ states are \textbf{not} copied.}
	\end{alertblock}
	}

\only<-3>{
\begin{block}<3->{Cups and Caps $=$ Compact Closed Category}
	\centering\tikzfig{cups-caps}
\end{block}}

\only<4->{
	\begin{block}{Universality}
		\centering\tikzfig{universality} (up to a global scalar)
	\end{block}
	$\dots$ but too much? 
	\begin{itemize}
		\item<5-> We have \textbf{postselection}, a.k.a. "assert(~)".
		\item<5-> We have states of norm (i.e. probability) greater than one.
	\end{itemize}
}
\end{frame}

\begin{frame}
	\frametitle{ZX Diagrams are Graphs}
	\twocl[-2cm]{
	\begin{exampleblock}{The Z-spider}
		\[
		\tikzfig{green-node}\]
		\[ \begin{matrix} \ket{0} & \mapsto & \ket{0} \otimes \dots \otimes \ket{0} \\ \ket{1} & \mapsto & \textbf{e}^{\textbf{i}\varphi}\ket{1} \otimes \dots \otimes \ket{1} \end{matrix} \]
	\end{exampleblock}
	\begin{block}{ZX-diagrams are multigraphs}
		\centering\tikzfig{multigraph}\\[0.2cm]
		$\implies$ Only the topology matters		
	\end{block}
	}{
	\begin{block}<2->{ZX-diagrams are simple graphs}
		\centering\tikzfig{simplegraph}\\[0.2cm]
		(up to a global scalar)\\
		$\implies$ Multi-edges are unnecessary	
	\end{block}
	\begin{block}<3->{ZX-diagrams are bipartite}		
		\centering\tikzfig{bipartite}\\[0.2cm]
		$\implies$ We can ensure colors alternate	
	\end{block}
	\uncover<3->{\textit{See [Vilmart et al. 2019] for the remaining equations.}}
}
\end{frame}


\begin{frame}
	\frametitle{Using the ZX-Calculus}\centering
	\includegraphics[height=7.5cm]{\figfile/zx.png}
\end{frame}

\begin{frame}
	\frametitle{And what About the "Too Much"?}
		\begin{enumerate}
		\item Write your quantum circuit
		\item Convert to ZX
		\item Optimize in ZX using the equations
		\begin{itemize}
			\item Extremely powerful
		\end{itemize}
		\item<2->[\alert{!!!}] \alert{Problem:} ZX-circuits are \textbf{not} physically implementable
\item<3-> Extraction of a new quantum circuit
\begin{itemize}
\item Highly non-trivial
\item Partially un-optimise the circuit
\end{itemize}
\item<3-> Run/Simulate the new quantum circuit
\end{enumerate}

\begin{block}<4->{What do we Optimise?}
	\begin{itemize}
		\item Total number of gates
		\item Number of ancillas / temporary quantum bits
		\item Some specific gates (T-gate, etc) hard to simulate
		\item Etc
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}
	\frametitle{Graphical Languages for Quantum Computation: Two Families}
	\twocl{
		\begin{block}{Tensor $\otimes$ family}
			$A \parallel B := A \otimes B$
			\begin{itemize}
				\item Quantum circuits
				\item ZX calculus
				\item \dots
				\item<3->[$\to$] Entanglement
			\end{itemize}
		\end{block}
	}{	
	\begin{exampleblock}<2->{Plus $\oplus$ family}
		$A \parallel B := A \oplus B$
		\begin{itemize}
			\item Optical experiments
			\item PBS calculus
			\item \dots
			\item<3->[$\to$] Generalized quantum control
		\end{itemize}
	\end{exampleblock}

	}
\twocl{
	\[\scalebox{2}{\tikzfig{circuit-example}}\]
}{
\uncover<2->{\[\scalebox{2}{\tikzfig{optic-example}}\]}}
\end{frame}
\begin{frame}
	\frametitle{The Tensor-Plus Calculus (Work In Progress): a Marriage}
	
	\[ \scalebox{1.5}{\tikzfig{circuit-and-optic}} \]
\twocl{
	\begin{alertblock}<2->{A union of both families}
		\begin{description}
			\item[Idea] $\parallel$ is both $\only<-3>{\otimes}\only<4->{\tensor}$ and $\oplus$.
			\item[Solution] Each wire can be either "enabled" or "disabled".
		\end{description}
	\end{alertblock}
}{
 \uncover<3->{$A \parallel B := (A \only<-3>{\otimes}\only<4->{\tensor} B) \oplus (A \only<-3>{\otimes}\only<4->{\tensor} \bfup{1}) \oplus (\bfup{1} \only<-3>{\otimes}\only<4->{\tensor} B) \oplus (\bfup{1} \only<-3>{\otimes}\only<4->{\tensor} \bfup{1})$}
}
\end{frame}


\begin{frame}
	\frametitle{The Geometry of Sheets}
	\twocl{
		\[ \tikzfig{SheetsWithWorlds} \qquad \rightsquigarrow \qquad \raisebox{-30pt}{\includegraphics{\figfile/WorldsWithSheets}} \]
	}{
		\[ \tikzfig{TwoTensorLong} \qquad \rightsquigarrow \qquad  \raisebox{-15pt}{\includegraphics{\figfile/TwoIdOneSheet}} \]
		\[ \qquad \]
		\[ \tikzfig{TwoPlusLong}\qquad \rightsquigarrow \qquad  \raisebox{-20pt}{\includegraphics{\figfile/TwoIdTwoSheets}} \]
	}
\end{frame}


\begin{frame}
	\frametitle{Examples of Encoding}
	\threecl{
		\begin{block}{The Hadamard gate}
			\[\tikzfig{Hadamard}\]
		\end{block}
	}{
		\begin{block}<2->{The Controlled-NOT gate}
			\[\tikzfig{CNOT}\]
		\end{block}
	}{
		\begin{exampleblock}<3->{The Polarising-Beam-Splitter}		
			\[\tikzfig{PBS}\]
	\end{exampleblock}}
\end{frame}



\begin{frame}
	\frametitle{Conclusion of the Tutorial}
	\twocl{
		\begin{block}{Quantum Circuits}
			Boolean circuits \textbf{except} that:
			\begin{itemize}
				\item No duplication
				\item Generated by C-NOT, H, P($\varphi$)
			\end{itemize}
		\end{block}
}{
\begin{block}{Important Properties}
	\begin{itemize}
		\item Universality
		\item Equational theory that is
		\begin{itemize}
			\item Sound
			\item \alert{Complete}
		\end{itemize}
	\end{itemize}
\end{block}
}
	
	\twocl{
		\begin{block}{Categories for Graphical Languages}
			\begin{itemize}
				\item Symmetric Monoidal Categories
				\item[] (more precisely colored PROP)
				\item ZX $\implies$ Compact Closed Category
			\end{itemize}
		\end{block}
	}{
		\begin{block}{Parallel Wires}
			\begin{itemize}
				\item Tensor $\otimes$ for Circuits/ZX/etc
				\item Direct Sum $\oplus$ for Optics/etc
				\item Both (WIP)
			\end{itemize}
		\end{block}
	}	
~\\\hfill \textit{Thank you for your attention.}
\end{frame}



\backupbegin
% Slides here do not count toward total page count
\begin{frame}
	\frametitle{UPDATE: Minimal Equational Theory [Clément et al. 2024]}
	
	\centering
	\includegraphics[width=15cm,trim={2cm 22.2cm 2cm 4.2cm},clip]{\figfile/UpdateCircuitsQuantiquesP2.pdf}
	\begin{center}
		\textit{The $R_X(\varphi)$ box and the multicontrolled $P(2\pi)$ are macros.\\The global phase are omitted.}
	\end{center}
\end{frame}

\backupend


\end{document}


